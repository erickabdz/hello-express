const express = require("express")
const app = express()

app.get("/hello", (request, response)=> {
    response.send("Hello world from /hello!")
})

app.post("/hello", (request, response) => {
    response.send("Hello Ericka from post /hello!")
})

app.listen(4000, () => {
    console.log("App is up and running congrats!")
}) // port + anonymous function for active status